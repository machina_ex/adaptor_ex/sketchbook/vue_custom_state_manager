// perfect way to build a globally accessible store from: https://dev.to/blacksonic/you-might-not-need-vuex-with-vue-3-52e4

import { reactive, provide, inject } from 'vue';

export const stateSymbol = Symbol('state');
export const createState = () => reactive(
  // this is the globally accessible and reactive stuff:
  { 
    counter: 0, 
    user: "Jonny Doe"
  } 
);

export const useState = () => inject(stateSymbol);
export const provideState = () => provide(
  stateSymbol, 
  createState()
);